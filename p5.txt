Práctica 5 - Sesión SIP
Protocolos para la Transmisión de Audio y Vı́deo en Internet
Versión 8.0.1 - 6.11.2017

Ejercicios

Creación de repositorio para la práctica

1. Con el navegador, dirı́gete al repositorio ptavi-p5 en la cuenta del
profesor en GitHub1 y realiza un fork, de manera que consigas tener
una copia del repositorio en tu cuenta de GitHub. Clona el repositorio
que acabas de crear a local para poder editar los archivos. Trabaja a
partir de ahora en ese repositorio, sincronizando los cambios que vayas
realizando.

Como tarde al final de la práctica, deberás realizar un push para subir
tus cambios a tu repositorio en GitHub. En esta práctica, al contrario
que con las demás, se recomienda hacer frecuentes commits, pero el
push al final.

Análisis de una sesión SIP

Se ha capturado una sesión SIP con Ekiga (archivo sip.cap.gz), que
se puede abrir con Wireshark2 . Se pide rellenar las cuestiones que se
plantean en este guión en el fichero p5.txt que encontrarás también
en el repositorio.

2. Observa que las tramas capturadas corresponden a una sesión SIP
con Ekiga, un cliente de VoIP para GNOME. Responde a las siguientes
cuestiones:
* ¿Cuántos paquetes componen la captura?
 - La componen 954 paquetes
* ¿Cuánto tiempo dura la captura?
 - Dura 56.149345 segundos.
* ¿Qué IP tiene la máquina donde se ha efectuado la captura? ¿Se
trata de una IP pública o de una IP privada? ¿Por qué lo sabes?
 - La IP de la máquina donde se ha efectuado la captura es la 192.168.1.34. Es una IP privada de clase C. Se sabe porque está entre los rangos 192.168.0.0 a 192.168.255.255 y una dirección IP dentro de estos rangos se considera no direccionable, puesto que no es exclusiva.

3. Antes de analizar las tramas, mira las estadı́sticas generales que aparecen en el menú de Statistics. En el apartado de jerarquı́a de protocolos (Protocol Hierarchy) se puede ver el porcentaje del tráfico
correspondiente al protocolo TCP y UDP.
* ¿Cuál de los dos es mayor? ¿Tiene esto sentido si estamos hablando
de una aplicación que transmite en tiempo real?
 - UDP tiene más porcentaje (96,2%) que TCP (2,1%).
* ¿Qué otros protocolos podemos ver en la jerarquı́a de protocolos?
¿Cuales crees que son señal y cuales ruido?
 - Otros protocolos visibles son IP.v4, CLASSIC-STUN, H.261, SIP, RTP, RTCP, DNS, HTTP, ICMP, ARP. Todos serían señal menos ICMP y ARP que son ruido.

4. Observa por encima el flujo de tramas en el menú de Statistics en IO
Graphs. La captura que estamos viendo incluye desde la inicialización
(registro) de la aplicación hasta su finalización, con una llamada entremedias.
* Filtra por sip para conocer cuándo se envı́an paquetes SIP. ¿En
qué segundos tienen lugar esos envı́os?
 - Se observa que en el segundo 7 se hace un registro; en el segudo 14 hay un intento de llamada que se repite en el segundo 16; en el segundo 38 se reciben los mensajes BYE y en el segundo 55 expira.
* Y los paquetes con RTP, ¿cuándo se envı́an?
 - Se envían entre los segundo 17 a 38.

[Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

5. Analiza las dos primeras tramas de la captura.
* ¿Qué servicio es el utilizado en estas tramas?
 - DNS
* ¿Cuál es la dirección IP del servidor de nombres del ordenador
que ha lanzado Ekiga?
 - 80.58.61.250
* ¿Qué dirección IP (de ekiga.net) devuelve el servicio de nombres?
 - 86.64.162.35

6. A continuación, hay más de una docena de tramas TCP/HTTP.
* ¿Podrı́as decir la URL que se está pidiendo?
 - http://ekiga.net/ip/
* ¿Qué user agent (UA) la está pidiendo?
 - Ekiga
* ¿Qué devuelve el servidor?
 - Devuelve el text/html solicitado, el cual contiene la dirección IP 83.36.48.212 .
* Si lanzamos el navegador web, por ejemplo, Mozilla Firefox, y
vamos a la misma URL, ¿qué recibimos? ¿Qué es, entonces, lo
que está respondiendo el servidor?
 - Recibo 212.128.255.31, dirección IP pública de la máquina en la  que estoy usando. 
   EL servidor lo que hace entonces es envíar la IP pública correspondiente a la privada que realiza la petición.

7. Hasta la trama 45 se puede observar una secuencia de tramas del
protocolo STUN.
* ¿Por qué se hace uso de este protocolo?
 - Para permitir establecer una conexión entre extremos cuando estos se encuentran detrás de enrutadores NAT, cosa que SIP no permite.
* ¿Podrı́as decir si estamos tras un NAT o no?
 - Sí estamos tras un NAT, ya que se ejecuta CLASSIC-STUN.

8. La trama 46 es la primera trama SIP. En un entorno como el de Internet, lo habitual es desconocer la dirección IP de la otra parte al
realizar una llamada. Por eso, todo usuario registra su localización en
un servidor Registrar. El Registrar guarda información sobre los
usuarios en un servidor de localización que puede ser utilizado para
localizar usuarios.
* ¿Qué dirección IP tiene el servidor Registrar?
 - 86.64.162.35
* ¿A qué puerto (del servidor Registrar) se envı́an los paquetes
SIP?
 -Se envían al puerto 5060
* ¿Qué método SIP utiliza el UA para registrarse?
 - Method: REGISTER
* Además de REGISTER, ¿podrı́as decir qué instrucciones SIP entiende el UA?
 - INVITE, ACK, OPTIONS, BYE, CANCEL, NOTIFY, REFER, MESSAGE.

[Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

9. Fijémonos en las tramas siguientes a la número 46:
* ¿Se registra con éxito en el primer intento?
 - No, muestra el mensaje Status-Line: SIP/2.0 401 Unauthorized
* ¿Cómo sabemos si el registro se ha realizado correctamente o no?
 - Se sabe que el registro se ha realizado correctamente cuando aparece el mensaje Status: 200 OK
* ¿Podrı́as identificar las diferencias entre el primer intento y el
segundo de registro? (fı́jate en el tamaño de los paquetes y mira
a qué se debe el cambio)
 - La primera trama (numero 50) tiene 714 bytes y la segunda (numero 54) tiene 664 bytes.
   La diferencia más significativa entre una y otra se observa en el Message Header, donde en la trama 50 hay un campo de WWW-Authenticate y en la trama 54 aparace el campo Contact en su lugar. 
* ¿Cuánto es el valor del tiempo de expiración de la sesión? Indica
las unidades.
 - Son 3600 segundos.

10. Una vez registrados, podemos efectuar una llamada. Vamos a probar
con el servicio de eco de Ekiga que nos permite comprobar si nos
hemos conectado correctamente. El servicio de eco tiene la dirección
sip:500@ekiga.net. Veamos el INVITE de cerca.
* ¿Puede verse el nombre del que efectúa la llamada, ası́ como su
dirección SIP?
 - Se puede ver en el campo From del Message Header, donde aparece:
   SIP Display info: "Gregorio Robles"
   SIP from address User Part: grex
   SIP from address Host Part: ekiga.net
* ¿Qué es lo que contiene el cuerpo de la trama? ¿En qué formato/protocolo está?
 - Contiene las descripción de la sesión. 
   Está bajo el protocolo SDP version 0.
* ¿Tiene éxito el primer intento? ¿Cómo lo sabes?
 - No porque en la siguiente trama se manda el mensaje Status: 407 Proxy Authentication Required
* ¿En qué se diferencia el segundo INVITE más abajo del primero?
¿A qué crees que se debe esto?
 - El primero (trama numero 84) contiene 982 bytes y el segundo (tama numero 103) contiene 1181 bytes y además lleva incluido el campo Proxy-Authorization.

11. Una vez conectado, estudia el intercambio de tramas.
* ¿Qué protocolo(s) se utiliza(n)? ¿Para qué sirven estos protocolos?
 - Se utilizan los siguientes protocolos:
   i) H.261: es un estándar ITU-T de compresión de video.
   ii) RTP: permite la transmisión de información en tiempo real, como audio y video.
* ¿Cuál es el tamaño de paquete de los mismos?
 - Las tramas H.261 contienen de 220-1078 bytes, mientras que las tramas RTP tienen siempre 214 bytes.
* ¿Se utilizan bits de padding?
 - No. Esto se observa en las tramas RTP con en campo ..0. .... = Padding: False.
* ¿Cuál es la periodicidad de los paquetes (en origen; nota que la
captura es en destino)?
 - La periocidad son 20ms, debido a que sabemos que en G.711 se mandan 8000 muestras y sabemos que en cada paquete se mandan 160 bytes al dividirlo encontramos que mandamos 50 paquetes por segundo, entonces cada paquete se enviaran cada 20 ms.
* ¿Cuántos bits/segundo se envı́an?
 -G.711(estándar de codificación de audio mostrado en las tramas RTPO) trabaja enviando a 64Kbps (64000 bits/segundo).

[Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

12. Vamos a ver más a fondo el intercambio RTP. En Telephony hay una
opción RTP. Empecemos mirando los flujos RTP.
* ¿Cuántos flujos hay? ¿por qué?
 - Hay dos flujos, ya que se manda audio(puerto 510) y video(puerto 5014).
* ¿Cuántos paquetes se pierden?
 - Ninguno.
* ¿Cuál es el valor máximo del delta? ¿Y qué es lo que significa el
valor de delta?
 - Para el audio  el valor máximo es 1290.444ms y para el video, 1290.479ms
   EL valor de delta es el tiempo que tarda en enviarse un paquete (latencia).
* ¿Cuáles son los valores de jitter (medio y máximo)? ¿Qué
quiere decir eso? ¿Crees que estamos ante una conversación de
calidad?
 - Audio(G.711) max jitter: 119.635, mean jitter:42.5
   Video(H.261) max jitter: 183.096, mean jitter:153.240
   El valor de jitter indica la variación del retraso en la recepción de paquetes. Como el valor de jitter es muy elevado, la conversación no es de calidad.

13. Elige un paquete RTP de audio. Analiza el flujo de audio en Telephony
-> RTP -> Stream Analysis.
* ¿Cuánto valen el delta y el jitter para el primer paquete que
ha llegado?
 - Los dos valen 0ms.
* ¿Podemos saber si éste es el primer paquete que nos han enviado?
 - Si, por el valor de su Sequence number.
* Los valores de jitter son menores de 10ms hasta un paquete
dado. ¿Cuál?
 - Hasta el paquete 247.
* ¿A qué se debe el cambio tan brusco del jitter?
 - Este cambio se debe al cambio de velocidad en la transmisión de los paquetes (delta aumenta bruscamente).
* ¿Es comparable el cambio en el valor de jitter con el del delta?
¿Cual es más grande?
 - El jitter depende de la delta por lo tanto es comparable que los dos aumenten
   Es mas grande el cambio del delta.

14. En Telephony selecciona el menú VoIP calls. Verás que se lista la
llamada de voz IP capturada en una ventana emergente. Selecciona
esa llamada y pulsa el botón Graph.
* ¿Cuánto dura la conversación?
 - Dura 24.803 segundos
* ¿Cuáles son sus SSRC? ¿Por qué hay varios SSRCs? ¿Hay CSRCs?
 - Video: 0x43306582; audio: 0xBF4AFD3.
   Hay varios porque se utilizan para los dos flujos.
   NO hay CSRCs ya que no están mezclados los flujos.

15. Identifica la trama donde se finaliza la conversación.
* ¿Qué método SIP se utiliza?
 - Method: BYE
* ¿En qué trama(s)?
 - En las tramas 924, 925, 927 y 933
* ¿Por qué crees que se envı́a varias veces?
 - Porque estamos usando UDP, que es un protocolo no fiable, y necesitamos que sepa que se ha acabado la conexión, por eso lo mandamos varias veces, asegurándonos que así le llegará si se pierde alguno. Además, se manda el mensaje de confirmación Status: 200 OK a partir de la trama 938.

16. Finalmente, se cierra la aplicación de VozIP.
* ¿Por qué aparece una instrucción SIP del tipo REGISTER?
 - Porque el usuario quiere eliminar su cuenta, por lo que pone el Expires a 0.
* ¿En qué trama sucede esto?
 - En la 950.
* ¿En qué se diferencia con la instrucción que se utilizó con anterioridad (al principio de la sesión)?
 - La diferencia es que al inicio de sesión el valor de Expires era 3600 segundos y al final, 0 segundos.

[Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

Captura de una sesión SIP

17. Dirı́gete a la web http://www.ekiga.net con el navegador y créate
una cuenta. Lanza Ekiga, y configúralo con los datos de la cuenta
que te acabas de crear. Comprueba que estás conectado (En la barra
al final de la ventana podrás ver “Connected”). Al terminar, cierra
completamente Ekiga.

18. Captura una sesión SIP de una conversación con el número SIP sip:500@ekigan.net.
Recuerda que has de comenzar a capturar tramas antes de arrancar
Ekiga para ver todo el proceso3 .

19. Observa las diferencias en el inicio de la conversación entre el entorno
del laboratorio y el del ejercicio anterior4 :
* ¿Se utilizan DNS y STUN? ¿Por qué?
* ¿Son diferentes el registro y la descripción de la sesión?

20. Identifica las diferencias existentes entre esta conversación y la conversación anterior:
* ¿Cuántos flujos tenemos?
* ¿Cuál es su periodicidad?
* ¿Cuánto es el valor máximo del delta y los valores medios y
máximo del jitter?
* ¿Podrı́as reproducir la conversación desde Wireshark? ¿Cómo?
Comprueba que poniendo un valor demasiado pequeño para el
buffer de jitter, la conversación puede no tener la calidad necesaria.
* ¿Sabrı́as decir qué tipo de servicio ofrece sip:500@ekiga.net?
[Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]


21. Filtra por los paquetes SIP de la captura y guarda únicamente los
paquetes SIP como p5.pcapng. Abre el fichero guardado para cerciorarte de que lo has hecho bien. Deberás añadirlo al repositorio.
[Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]
[Al terminar la práctica, realiza un push para sincronizar tu repositorio GitHub]

